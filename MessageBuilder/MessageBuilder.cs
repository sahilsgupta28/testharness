﻿/**
 * Message Builder
 * Functions to Creates a new message
 * Test Message denoting all fields of test request to be included in message payload while communicating
 * 
 * FileName     : MessageBuilder.cs
 * Author       : Sahil Gupta
 * Date         : 12 October 2016 
 * Version      : 1.0
 * 
 * Public Interface for Message Builder
 * -------------------------------------
 * MessageBuilder(Message.MessageType Type)
 *  - Initialize new message builder
 * Message BuildMessage(string from, string to, string author, string payload)
 *  - Initialize fields of message
 * Message GetMessage()
 *  - Get message object
 * void DisplayMessage(Message msg)
 *  - Display message fields on console
 * 
 * Public Interface for TestMessage
 * ----------------
 * TestMessage()
 *  - new test message
 * TestMessage(string name)
 *  - new test message with test name
 * void addDriver(string name)
 *  - add driver in test message
 * void addCode(string name)
 *  - add code in test message
 * 
 * string GetTestRequestXML()
 *  - build new XML for test request
 * void ParseTestRequestXML(string TestRequestXmlString)
 *  - parse XML for extracting test request
 * string QueryTestResultXML()
 *  - build new XML to query test result
 * void ParseQueryTestResultXML(string TestResultXML)
 *  - parse XML for extracting query for test result
 * string GetTestResultXML(bool status, string Log)
 *  - build XML for test result
 * void ParseTestResultXML(string TestResultXML)
 *  - parse XML to extract test result
 *  
 * void DisplayResult()
 *  - display test message on console
 * string GetDetailedTestResult()
 *  - get string with all test message fields values
 * 
 * Build Process
 * -------------
 * - Required files:   ITest.cs, XMLParser.cs
 * 
 * Maintenance History
 * -------------------
 * ver 1.0 : 12 October 2016 
 *  - first release
 */

using System;
using System.Linq;
using System.Xml.Linq;
using System.IO;
using System.Collections.Generic;

namespace Communication
{
    using TestInterface;
    using XMLParser;
     
    public class MessageBuilder
    {
        Message msg;

        public MessageBuilder(Message.MessageType Type)
        {
            msg = new Message();
            msg.Type = Type;
        }

        public Message BuildMessage(string from, string to, string author, string payload)
        {
            msg.FromUrl = from;
            msg.ToUrl = to;
            msg.TimeStamp = DateTime.Now;
            msg.Author = author;
            msg.Payload = payload;

            return msg;
        }

        public Message GetMessage()
        {
            return msg;
        }
        
        public static void DisplayMessage(Message msg)
        {
            Console.WriteLine("From : {0}",msg.FromUrl);
            Console.WriteLine("To : {0}", msg.ToUrl);
            Console.WriteLine("TimeStamp : {0}", msg.TimeStamp);
            Console.WriteLine("Author : {0}", msg.Author);
            Console.WriteLine("Type: {0}", msg.Type.ToString());
            Console.WriteLine("Payload : {0}", msg.Payload);
        }

        static void Main(string[] args)
        {
            MessageBuilder TestRequestMsg = new MessageBuilder(Message.MessageType.TestRequestMessage);

            string payload = "TestRequest1";
            TestRequestMsg.BuildMessage("localhost", "localhost", "Sahil", payload);

            Console.WriteLine("Message Type : {0}", TestRequestMsg.GetMessage().Type.ToString());
            Console.WriteLine("Payload : {0}", TestRequestMsg.GetMessage().Payload);
        }
    }

    public class TestMessage
    {
        public int Version { get; set; }
        public string Author { get; set; }
        public DateTime TimeStamp { get; set; }
        public TimeSpan duration { get; set; }
        public string TestName { get; set; }
        public string TestDriver { get; set; }
        public List<string> TestCodes { get; set; }
        public bool TestStatus { get; set; }
        public string TestResult { get; set; }
        //string DateTimeFormat = "yyMMdd-HHmmss-fff";

        public TestMessage()
        {
            TimeStamp = DateTime.Now;
            TestCodes = new List<string>();
        }

        public TestMessage(string name)
        {
            TestName = name;
            TimeStamp = DateTime.Now;
            TestCodes = new List<string>();
        }

        public void addDriver(string name)
        {
            TestDriver = name;
        }

        public void addCode(string name)
        {
            TestCodes.Add(name);
        }

        public string GetTestRequestXML()
        {
            string TestRequest;

            TestRequest = "<?xml version=\"1.0\" encoding=\"utf-8\" ?>";
            TestRequest += "<TestRequest>";
            TestRequest += "<Version>1</Version>";
            TestRequest += "<Author>" + Author + "</Author>";
            TestRequest += "<TimeStamp>" + TimeStamp + "</TimeStamp>";
            TestRequest += "<Test Name=\"" + TestName + "\">";
            TestRequest += "  <TestDriver>" + Path.GetFileName(TestDriver) + "</TestDriver>";
            foreach (string code in TestCodes)
                TestRequest += "  <Library>" + Path.GetFileName(code) + "</Library>";
            TestRequest += "</Test>";
            TestRequest += "</TestRequest>";

            return TestRequest;
        }

        public void ParseTestRequestXML(string TestRequestXmlString)
        {
            //call XML parser to parse string
            XmlParser Parser = new XmlParser();
            Parser.ParseXmlString(TestRequestXmlString);

            //fill test message structure with parsed info
            Version = Parser._xmlTestInfoList[0]._Version;
            Author = Parser._xmlTestInfoList[0]._Author;
            TimeStamp = Parser._xmlTestInfoList[0]._TimeStamp;
            TestName = Parser._xmlTestInfoList[0]._TestName;
            TestDriver = Parser._xmlTestInfoList[0]._TestDriver;

            foreach (var Library in Parser._xmlTestInfoList[0]._TestCode)
            {
                TestCodes.Add(Library);
            }
        }

        public string QueryTestResultXML()
        {
            string TestResultXml;
            TestResultXml = "<?xml version=\"1.0\" encoding=\"utf-8\" ?>";
            TestResultXml += "<TestQuery>";
            TestResultXml += "<Author>" + Author + "</Author>";
            TestResultXml += "<TestName>" + TestName + "</TestName>";
            TestResultXml += "</TestQuery>";
            return TestResultXml;
        }

        public void ParseQueryTestResultXML(string TestResultXML)
        {
            XDocument _xDoc = XDocument.Parse(TestResultXML);
            Author = _xDoc.Descendants("Author").First().Value;
            TestName = _xDoc.Descendants("TestName").First().Value;
        }

        public string GetTestResultXML(bool status, string Log)
        {
            string TestResultXml;

            TestResultXml = "<?xml version=\"1.0\" encoding=\"utf-8\" ?>";
            TestResultXml += "<TestResult>";
            TestResultXml += "<Author>" + Author + "</Author>";
            TestResultXml += "<TimeStamp>" + TimeStamp + "</TimeStamp>";
            TestResultXml += "<Test Name=\"" + TestName + "\">";
            TestResultXml += "  <TestDriver>" + Path.GetFileName(TestDriver) + "</TestDriver>";
            foreach (string code in TestCodes)
            {
                TestResultXml += "  <Library>" + Path.GetFileName(code) + "</Library>";
            }
            TestResultXml += "<TestStatus>" + status.ToString() + "</TestStatus>";
            TestResultXml += "<Log>" + Log + "</Log>";
            TestResultXml += "</Test>";
            TestResultXml += "</TestResult>";

            return TestResultXml;
        }

        public void ParseTestResultXML(string TestResultXML)
        {
            XDocument _xDoc = XDocument.Parse(TestResultXML);
            Author = _xDoc.Descendants("Author").First().Value;
            TimeStamp = DateTime.Parse(_xDoc.Descendants("TimeStamp").First().Value);
            XElement[] xElement = _xDoc.Descendants("Test").ToArray();

            foreach (var item in xElement)
            {
                TestName = item.Attribute("Name").Value;
                TestDriver = item.Element("TestDriver").Value;
                IEnumerable<XElement> xTestCode = item.Elements("Library");
                foreach (var library in xTestCode)
                {
                    TestCodes.Add(library.Value);
                }
                TestStatus = Convert.ToBoolean(item.Element("TestStatus").Value);
                TestResult = item.Element("Log").Value;
            }
        }

        public void DisplayResult()
        {
            Console.WriteLine("  {0,-12} : {1}", "Version", Version);
            Console.WriteLine("  {0,-12} : {1}", "Author", Author);
            Console.WriteLine("  {0,-12} : {1}", "TimeStamp", TimeStamp);
            Console.WriteLine("  {0,-12} : {1}", "Latency", duration);
            Console.WriteLine("  {0,-12} : {1}", "TestName", TestName);
            Console.WriteLine("  {0,-12} : {1}", "TestDriver", TestDriver);
            foreach (string Library in TestCodes)
            {
                Console.WriteLine("  {0,-12} : {1}", "Library", Library);
            }
            Console.WriteLine("  {0,-12} : {1}", "TestStatus", (TestStatus ? "PASS" : "FAIL"));
            Console.WriteLine("-----------Test Result-----------");
            Console.WriteLine("{0}", TestResult);
            Console.WriteLine("--------------------------------");
            Console.WriteLine("");
        }

        public string GetDetailedTestResult()
        {
            string Log= null;

            Log += "Author : " + Author;
            Log += "\nTestName : " + TestName;
            Log += "\nTimeStamp : " + TimeStamp;
            Log += "\nTestDriver : " + TestDriver;
            foreach (string Library in TestCodes)
                Log += "\nLibrary : " + Library;
            Log += "\nTestStatus : " + (TestStatus?"PASS":"FAIL");
            Log += "\n\nTestResult : \n" + TestResult;
            Log += "\nLatency : " + duration;

            return Log;
        }
    }
}
