﻿///////////////////////////////////////////////////////////////////////
///  HiResTimer.cs - High Resolution Timer - Uses Win32             ///
///  ver 1.0         Performance Counters and .Net Interop          ///
///                                                                 ///
///  Language:     Visual C#                                        ///
///  Platform:     Dell Dimension 8100, Windows Pro 2000, SP2       ///
///  Application:  CSE681 Example                                   ///
///  Author:       Jim Fawcett, CST 2-187, Syracuse Univ.           ///
///                (315) 443-3948, jfawcett@twcny.rr.com            ///
///////////////////////////////////////////////////////////////////////
/// Based on:                                                       ///
/// Windows Developer Magazine Column: Tech Tips, August 2002       ///
/// Author: Shawn Van Ness, shawnv@arithex.com                      ///
///////////////////////////////////////////////////////////////////////

/**
 * Public Interface
 * ----------------
 * Timer()
 * void Start()
 * ulong Stop()
 * ulong GetElapsedTicks()
 * ulong GetElapsedMicerSec()
 * TimeSpan GetTimeSpan()
 * 
 * Build Process
 * -------------
 * - Required files:   NA
 * 
 * Maintenance History
 * -------------------
 * ver 1.0
 *  - first release
 */

using System;
using System.Runtime.InteropServices; // for DllImport attribute
using System.ComponentModel; // for Win32Exception class
using System.Threading; // for Thread.Sleep method

namespace nsTimer
{
   public class Timer
   {
     ulong a, b, f;
     
     public Timer()
      {
         a = b = 0UL;
         if ( QueryPerformanceFrequency( out f) == 0) 
            throw new Win32Exception();
      }

      private ulong ElapsedTicks
      {
         get
         { return (b-a); }
      }

        public ulong GetElapsedTicks()
        {
            return ElapsedTicks;
        }

        private ulong ElapsedMicroseconds
      {
         get
         { 
            ulong d = (b-a); 
            if (d < 0x10c6f7a0b5edUL) // 2^64 / 1e6
               return (d*1000000UL)/f; 
            else
               return (d/f)*1000000UL;
         }
      }

        public ulong GetElapsedMicerSec()
        {
            return ElapsedMicroseconds;
        }

      private TimeSpan ElapsedTimeSpan
      {
         get
         { 
            ulong t = 10UL*ElapsedMicroseconds;
            if ((t&0x8000000000000000UL) == 0UL)
               return new TimeSpan((long)t);
            else
               return TimeSpan.MaxValue;
         }
      }

        public TimeSpan GetTimeSpan()
        {
            return ElapsedTimeSpan;
        }

      private  ulong Frequency
      {
         get
         { return f; }
      }

      public void Start()
      {
         Thread.Sleep(0);
         QueryPerformanceCounter( out a);
      }

      public ulong Stop()
      {
         QueryPerformanceCounter( out b);
         return ElapsedTicks;
      }

     // Here, C# makes calls into C language functions in Win32 API
     // through the magic of .Net Interop

      [ DllImport("kernel32.dll", SetLastError=true) ]
      protected static extern 
         int QueryPerformanceFrequency( out ulong x);

      [ DllImport("kernel32.dll") ]
      protected static extern 
         int QueryPerformanceCounter( out ulong x);

        public static void Main()
        {
            Timer timer = new Timer();

            Console.WriteLine("Starting Timer");
            timer.Start();

            Console.WriteLine("Starting 1 sec delay");
            Thread.Sleep(1000);
            
            timer.Stop();
            Console.WriteLine("Stopping Timer");

            Console.WriteLine("\nFrequency : ({0}) ", timer.Frequency);

            Console.WriteLine("\nTicks Elapsed : ({0})", timer.ElapsedTicks);
            Console.WriteLine("Time Elapsed : ({0}) micro sec", timer.GetElapsedMicerSec());
            Console.WriteLine("Time Span Elapsed : ({0})", timer.GetTimeSpan());
        }
    }
}