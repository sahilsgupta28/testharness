﻿/**
 * Client
 * A remote process instance that sends files to test harness for testing
 *      
 * FileName     : Client.cs
 * Author       : Sahil Gupta
 * Date         : 16 November 2016 
 * Version      : 1.0
 * 
 * Public Interface
 * ----------------
 * Client(string MyUrl)
 *  - create new instance of client sending requests from specified URL
 * void ConfigureURL(string TestHarnessUrl, string RepositoryUrl, string FileHostingUrl)
 *  - set URL path for repo and test harness
 * void SendTestRequest(TestMessage tMsg)
 *  - send new test request to test harness
 * void GetTestResult(TestMessage tMsg)
 *  - send new query for test result to test harness
 * Message ProcessTestResult()
 *  - get message containing test result
 *  
 * Build Process
 * -------------
 * - Required files: ITest.cs, BlockingQueue.cs, MessageBuilder.cs, MsgParser.cs, Sender.cs, Receiver.cs FileStreamer.cs
 * 
 * Maintenance History
 * -------------------
 * ver 1.0 : 16 November 2016
 *  - first release
 */

using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading;

namespace nsClient
{
    using TestInterface;
    using Communication;
    using nsBlockingQueue;
    using FileHosting;

    public class Client
    {
        string URL;
        string TestHarnessURL;
        string RepositoryURL;

        Thread tSender;
        Sender MySender;
        BlockingQueue<Message> SendQ;

        FileStreamer Streamer;

        Thread tReceiver;
        Receiver MyReceiver;
        BlockingQueue<Message> ReceiveQ;

        BlockingQueue<Message> ResultQ;

        public Client(string MyUrl)
        {
            Console.WriteLine("Starting Client at URL ({0})", MyUrl);
            URL = MyUrl;

            ResultQ = new BlockingQueue<Message>();

            // Create and start receiver to recieve messages
            ReceiveQ = new BlockingQueue<Message>();
            MyReceiver = new Receiver();
            tReceiver = new Thread(ThreadReceiver);
            tReceiver.Start();

            // create and start sender to send message
            SendQ = new BlockingQueue<Message>();
            MySender = new Sender();
            tSender = new Thread(ThreadSender);
            tSender.Start();
        }

        public void ConfigureURL(string TestHarnessUrl, string RepositoryUrl, string FileHostingUrl)
        {
            TestHarnessURL = TestHarnessUrl;
            RepositoryURL = RepositoryUrl;
            Streamer = new FileStreamer(FileHostingUrl);
        }

        public void SendTestRequest(TestMessage tMsg)
        {
            UploadFilesToRepo(tMsg);

            MessageBuilder TestRequestMsg = new MessageBuilder(Message.MessageType.TestRequestMessage);
            TestRequestMsg.BuildMessage(URL, TestHarnessURL, tMsg.Author, tMsg.GetTestRequestXML());

            Console.WriteLine("REQUIREMENT 2: Sending Test Request to Test Harness after Sending DLL Files");
            Console.WriteLine("REQUIREMENT 6: Sending Test Request to Test Harness...");

            //Enqueue message in send queue for sender thread to process
            SendQ.enQ(TestRequestMsg.GetMessage());
        }

        private void UploadFilesToRepo(TestMessage tMsg)
        {
            if (tMsg.TestDriver.Contains("ExceptionTestDriver.dll"))
            {
                Console.WriteLine("For requirement 3, not uploading files to repository");
                return;
            }

            Console.WriteLine("REQUIREMENT 2: Sending DLL files to Repository before sending Test Request...");
            Console.WriteLine("REQUIREMENT 6: Sending test driver & code DLL files to Repository...");
            
            //Send Test Driver File to Repository
            Console.WriteLine("REQUIREMENT 6: Uploading Test Driver file ({0}) to Repository using Streaming.", tMsg.TestDriver);
            Streamer.UploadFile(tMsg.TestDriver);

            //Send Test Code Files to Repository
            foreach (var Library in tMsg.TestCodes)
            {
                Console.WriteLine("REQUIREMENT 6: Uploading Code file ({0}) to Repository using Streaming.", Library);
                Streamer.UploadFile(Library);
            }
        }

        public void GetTestResult(TestMessage tMsg)
        {
            MessageBuilder TestLogMsg = new MessageBuilder(Message.MessageType.GetTestLogMsg);
            TestLogMsg.BuildMessage(URL, RepositoryURL, tMsg.Author, tMsg.QueryTestResultXML());

            Console.WriteLine("REQUIREMENT 9: Query for Test Log for Author({0}) with Test Name({1})", tMsg.Author, tMsg.TestName);

            //Enqueue message in send queue for sender thread to process
            SendQ.enQ(TestLogMsg.GetMessage());
        }

        private void ThreadSender()
        {
            while (true)
            {
                //Wait for new message from user
                Message msg = SendQ.deQ();

                //Connect to process to which we need to send message
                MySender.CreateNewSession(msg.ToUrl);

                //Send message using WCF
                Console.WriteLine("REQUIREMENT 10: Sending Message ({0}) to {1} using WCF", msg.Type.ToString(), msg.ToUrl);
                MySender.SendMessage(msg);
            }
        }

        private void ThreadReceiver()
        {
            MyReceiver.Init(URL);

            while (true) {
                //Wait for acknowledgement messages for sent requests
                Console.WriteLine("Waiting for Messages...");
                Message Msg = MyReceiver.GetMessage();

                switch (Msg.Type) {
                    case Message.MessageType.TestRequestAck:
                    case Message.MessageType.TestLogsAck:

                        Console.WriteLine("\nREQUIREMENT 10: New ({0}) Message received from ({1}) using WCF.", Msg.Type.ToString(), Msg.FromUrl);
                        Console.WriteLine("REQUIREMENT 6 & 7 : Got Test logs from Test Harness.");

                        //Parse result form message
                        TestMessage tMsg = new TestMessage();
                        tMsg.ParseTestResultXML(Msg.Payload);
                        tMsg.duration = DateTime.Now.Subtract(Msg.TimeStamp);
                        Console.WriteLine("REQUIREMENT 12: Overall Latency for sending message and receiving reply : {0}", tMsg.duration);

                        tMsg.DisplayResult();

                        //Enqueue for GUI window to process
                        ResultQ.enQ(Msg);
                        break;
                    default:
                        Console.WriteLine("Unknown Message ({0}) Received:", Msg.Type.ToString());
                        break;
                }
            }
        }

        /**
         *  Function call for GUI to get test result.
         */
        public Message ProcessTestResult()
        {
            return ResultQ.deQ();
        }

        static void Main(string[] args)
        {
            string MyUrl = @"http://localhost:4000/ICommunication/Client";
            string TestHarnessUrl = @"http://localhost:4000/ICommunication/TestHarness";
            string RepoUrl = @"http://localhost:4000/ICommunication/Repository";
            string FileHostingUrl = @"http://localhost:4000/IStream/FileHosting";

            Client MyClient = new Client(MyUrl);
            MyClient.ConfigureURL(TestHarnessUrl, RepoUrl, FileHostingUrl);

            Console.Title = "Console Client";
            Thread.Sleep(1000);

            while (true)
            {
                string TestName;

                /* 1. New Test Request */
                Console.WriteLine("\nNew Test Request");
                Console.WriteLine("Enter TestName: ");
                TestName = Console.ReadLine();
                //TestName = "test1";

                TestMessage NewTestRequest = new TestMessage(TestName);

                Console.WriteLine("Enter Author: ");
                NewTestRequest.Author = Console.ReadLine();
                //NewTestRequest.Author = "Sahil Gupta";

                Console.WriteLine("Enter Driver: ");
                NewTestRequest.addDriver(Console.ReadLine());
                //NewTestRequest.addDriver(@"E:\Sahil\Syracuse\Study\CSE 681 - SMA\Project\Project 4\Repository\ClientDir\ExceptionTestDriver.dll");
                
                Console.WriteLine("Enter Code: ");
                NewTestRequest.addCode(Console.ReadLine());
                //NewTestRequest.addCode(@"E:\Sahil\Syracuse\Study\CSE 681 - SMA\Project\Project 4\Repository\ClientDir\SampleCode.dll");

                MyClient.SendTestRequest(NewTestRequest);

                /* TODO : 2. Query past test */
                Console.WriteLine("\nQuery Test Results");
                Console.WriteLine("Enter TestName: ");
                TestName = Console.ReadLine();
                TestMessage NewQueryTestRequest = new TestMessage(TestName);

                Console.WriteLine("Enter Author: ");
                NewQueryTestRequest.Author = Console.ReadLine();
                MyClient.GetTestResult(NewQueryTestRequest);
            }
        }
    }
}
