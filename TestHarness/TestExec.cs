﻿/**
 * Test Executive
 * Test Harness program to execute test drivers in isolated Application Domains
 * Accepts test requests from remote clients
 * Communicates with remote repository to send/recieve test files and store test result
 *      
 * FileName     : TestExec.cs
 * Author       : Sahil Gupta
 * Date         : 24 September 2016 
 * Version      : 2.0
 * 
 * Public Interface
 * ----------------
 * TestExec(string MyUrl, string path)
 *  - Constructor to create new test harness instance listening 
 *    for test request at specified URL
 * void configureRepository(string RepoUrl, string FileHostingUrl)
 *  - Configure repository settings to use with this test harness
 * void ProcessTestRequests()
 *  - Starts processing of test request
 * void GetTestRequestResult(string TestRequest)
 *  - Displays result of a particular test request
 * void GetAuthorTestResult(string Author)
 *  - Gets all test results of a particular author
 * public void GetTestsSummary()
 *  - Displays a summary of all tests executed till date
 *
 * Build Process
 * -------------
 * - Required files:   
 *   ITest.cs, AppDomainMgr.cs, XMLParser.cs, BlockingQueue.cs, Timer.cs, FileMgr.cs
 *   MessageBuilder.cs, MsgParser.cs, Sender.cs, Receiver.cs
 * 
 * Maintenance History
 * -------------------
 * ver 1.0 : 24 September 2016
 *  - first release
 * ver 2.0 : 20 November 2016
 *  - Added communication path to communicate with repository to get test files
 *  - Added communication path to communicate with client to accept test files
 */

using System;
using System.IO;
using System.Reflection;
using System.Collections.Generic;
using System.Threading;

namespace TestHarness
{
    using XMLParser;
    using FileManager;
    using Communication;
    using TestInterface;
    using nsBlockingQueue;
    using nsTimer;

    public class TestExec
    {
        /**********************************************************************
                                 M E M B E R S
         **********************************************************************/

        string URL;
        string RepositoryPath;
        string RepoServerUrl;
        string FileHostingUrl;

        /**
         * Define queue that will serialize test requests
         * The queue holds xml file path denoting a test request
         * 
         * Do we want this queue to be static? We could have multiple TestExec instances 
         * but would want only one queue to manage test requests
         */
        Thread tReceiver;
        Receiver MyReceiver;
        BlockingQueue<Message> ReceiveQ;

        Thread tSender;
        Sender MySender;
        BlockingQueue<Message> SendQ;

        Action<Message, bool, string> TestResultPublisher;

        /**********************************************************************
                                 P U B L I C   M E T H O D S
         **********************************************************************/

        public TestExec(string MyUrl, string path)
        {
            Console.WriteLine("REQUIREMET 11: Starting Test Harness at URL({0})", MyUrl);

            URL = MyUrl;
            RepositoryPath = path;

            /* Start Sender first as we should be able to send messages 
             * before starting to service */
            SendQ = new BlockingQueue<Message>();
            MySender = new Sender();
            tSender = new Thread(ThreadSender);
            tSender.Start();

            ReceiveQ = new BlockingQueue<Message>();
            MyReceiver = new Receiver();
            tReceiver = new Thread(ThreadReceiver);
            tReceiver.Start();

            TestResultPublisher = StoreTestResult;
            TestResultPublisher += AckTestRequest;
        }

        ~TestExec()
        {
            /* Stop Reciever first because if we stop sender and we recieve a msg,
             * then we cannot send the results of that msg */
            tReceiver.Join();

            tSender.Join();
        }

        public void configureRepository(string RepoUrl, string FileHostingUrl)
        {
            this.RepoServerUrl = RepoUrl;
            this.FileHostingUrl = FileHostingUrl;
        }

        /**
         * EnqueueTestRequest
         * Places a Test Request in a Queue
         */
        void EnqueueTestRequest(Message msg)
        {
            ReceiveQ.enQ(msg);
            Console.WriteLine("REQUIREMENT 4 : En-queued Request from ({0})", msg.Author);
        }

        /**
         * DequeueTestRequest
         * Removes a test request from queue
         */
        Message DequeueTestRequest()
        {
            Message msg;

            msg = ReceiveQ.deQ();
            Console.WriteLine("REQUIREMENT 4 : De-queued request from ({0})", msg.Author);

            return msg;
        }

        /**
         * ProcessTestRequests
         * Starts processing of test request
         */
        public void ProcessTestRequests()
        {
            try
            {
                while (true)
                {
                    Thread NewTest = new Thread((TR) =>
                    { /* Define thread to process test */
                        Message msg = TR as Message;

                        Console.WriteLine("REQUIREMENT 4 : Creating New Thread, ID({0})", Thread.CurrentThread.ManagedThreadId);
                        /* Pass test request to app domain */
                        AppDomainMgr AppDMgr = new AppDomainMgr(RepositoryPath, FileHostingUrl);
                        nsTimer.Timer MyTimer = new nsTimer.Timer();
                        MyTimer.Start();
                        AppDMgr.ProcessTestRequest(msg, TestResultPublisher);
                        MyTimer.Stop();
                        Console.WriteLine("REQUIREMENT 12 : Executed Test in ({0}) microseconds", MyTimer.GetElapsedMicerSec());
                    });

                    NewTest.Start(DequeueTestRequest()); /* De-queue Test Request and start thread */
                }
            }
            catch(Exception Ex)
            {
                Console.WriteLine("Exception : {0}", Ex.Message);
            }
        }

        void AckTestRequest(Message Msg, bool TestStatus, string TestResult)
        {
            TestMessage TestMsg = new TestMessage();
            TestMsg.ParseTestRequestXML(Msg.Payload);
            string TestResultXml = TestMsg.GetTestResultXML(TestStatus, TestResult);

            MessageBuilder TestRequestAckMsg = new MessageBuilder(Message.MessageType.TestRequestAck);
            TestRequestAckMsg.BuildMessage(URL, Msg.FromUrl, Msg.Author, TestResultXml);
            TestRequestAckMsg.GetMessage().TimeStamp = Msg.TimeStamp;

            Console.WriteLine("REQUIREMENT 6 & 10: Sending reply for test request to Client..");
            Console.WriteLine("REQUIREMENT 7: Sending test logs to Client..");
            SendQ.enQ(TestRequestAckMsg.GetMessage());
        }

        void StoreTestResult(Message Msg, bool TestStatus, string TestResult)
        {
            TestMessage TestMsg = new TestMessage();
            TestMsg.ParseTestRequestXML(Msg.Payload);
            string TestResultXml = TestMsg.GetTestResultXML(TestStatus, TestResult);

            MessageBuilder LogStore = new MessageBuilder(Message.MessageType.StoreTestLogMsg);
            LogStore.BuildMessage(URL, RepoServerUrl, Msg.Author, TestResultXml);

            Console.WriteLine("REQUIREMENT 7: Sending message to store test logs to repository..");
            SendQ.enQ(LogStore.GetMessage());
        }

        /**
         * GetTestRequestResult
         * Displays result of a particular test request
         */
        public void GetTestRequestResult(string TestRequest)
        {
            bool bRet;
            FileMgr Database = new FileMgr(RepositoryPath);

            XmlParser Parser = new XmlParser();
            bRet = Parser.ParseXmlFile(TestRequest);
            if (false == bRet)
            {
                Console.WriteLine("Error: Parser.ParseTestRequest({0})...FAILED", TestRequest);
                return;
            }

            /** 
             * CASE 1 : Test is not yet executed
             * We may want to check in queue if the test is not yet executed
             * 
             * Acquire Lock
             * Check if TestRequest is in Queue
             * If yes, release lock and Return
             */

            /**
             * CASE 2: Test has finished execution
             * In this case we have the results on log storage. 
             * Try accessing storage..
             */
            Console.WriteLine("REQUIREMENT 6 & 8 : CLIENT QUERY");
            bRet = Database.GetDriverTestResult(Parser._xmlTestInfoList[0]._Author, Parser._xmlTestInfoList[0]._TestDriver);
            if(true == bRet)
            {
                return;
            }

            /** 
             * CASE 3: Test is still executing
             * Return proper error saying execution is in progress
             */
        }

        /**
         * GetAuthorTestResult
         * Gets all test results of a particular author
         */
        public void GetAuthorTestResult(string Author)
        {
            FileMgr Database = new FileMgr(RepositoryPath);

            Console.WriteLine("REQUIREMENT 8 : ENTIRE LOG FILE (Authors Test)");
            Database.DisplayAuthorTestDetails(Author);
        }

        /**
         * GetTestsSummary
         * Displays a summary of all tests executed till date
         */
        public void GetTestsSummary()
        {
            FileMgr Database = new FileMgr(RepositoryPath);

            Console.WriteLine("REQUIREMENT 8 : SIMPLE SUMMARY INFORMATION");
            Database.DisplayTestSummary();
        }

        private void ThreadSender()
        {
            while (true)
            {
                Message msg = SendQ.deQ();

                Console.WriteLine("REQUIREMENT 10: Sending Message ({0}) to ({1}) using WCF.", msg.Type.ToString(), msg.ToUrl);
                MySender.CreateNewSession(msg.ToUrl);
                MySender.SendMessage(msg);
            }
        }

        private void ThreadReceiver()
        {
            MyReceiver.Init(URL);

            MessageParser Parser = new MessageParser();
            while (true)
            {
                Console.WriteLine("Waiting for Messages...");
                Message Msg = MyReceiver.GetMessage();

                Console.WriteLine("\nREQUIREMENT 10: New Message ({0}) Received from ({1}) using WCF", Msg.Type.ToString(), Msg.FromUrl);
                TimeSpan duration = DateTime.Now.Subtract(Msg.TimeStamp);
                Console.WriteLine("REQUIREMENT 12: Communication Latency for recieving message from client : {0}", duration);
                Console.WriteLine("REQUIREMENT 2 : Test Harness accepting message in XML form:");
                Parser.ParseMessage(Msg);

                /* Todo : check message type */
                EnqueueTestRequest(Msg);
            }
        }

        static void Main(string[] args)
        {    
            string Myurl = @"http://localhost:4000/ICommunication/TestHarness";
            string LocalRepoPath = @"Repository\TestHarnessDir";

            string RepoUrl = @"http://localhost:4000/ICommunication/Repository";
            string FileHostingUrl = @"http://localhost:4000/IStream/FileHosting";

            Console.Title = "Test Harness (" + Myurl + ")";

            TestExec testexe = new TestExec(Myurl, LocalRepoPath);
            testexe.configureRepository(RepoUrl, FileHostingUrl);
            testexe.ProcessTestRequests();
        }
    }
}