﻿/**
 * File Streamer
 * Provides File Streaming service to send and recieve files from a host processes
 * 
 * FileName     : FileStreamer.cs
 * Author       : Sahil Gupta
 * Date         : 16 November 2016 
 * Version      : 1.0
 *
 * Public Interface for Client
 * ----------------------------
 * FileStreamer(string url, int BlockSize = 1024)
 *  - Constructor used by client to connect to a host process
 * void UploadFile(string FilePath)
 *  - Upload file to host
 * void DownloadFile(string SavePath, string Filename)
 *  - download file from host
 * bool CheckFileAvailability(string Filename)
 *  - check if file is present on host
 * 
 * Public Interface for Server
 * ----------------------------
 * static FileServiceHost NewHost(string URL, string RepoPath, int BlkSize = 1024)
 *  - Create new instance for host process
 *  
 * Build Process
 * -------------
 * - Required files:   ITest.cs
 * 
 * Maintenance History
 * -------------------
 * ver 1.0 : 16 November 2016
 *  - first release
 */

using System;
using System.IO;
using System.ServiceModel;
using System.ServiceModel.Channels;
using System.Threading;

namespace FileHosting
{
    using TestInterface;

    public class FileStreamer
    {
        IStream Channel;
        int BlockSize;
        byte[] Block;

        public FileStreamer(string url, int BlockSize = 1024)
        {
            Channel = null;
            this.BlockSize = BlockSize;
            Block = new byte[this.BlockSize];
            ConnectTo(url);
        }

        ~FileStreamer()
        {
            if (null != Channel)
            {
                ((IChannel)Channel).Close();
            }
        }

        private void ConnectTo(string url)
        {
            BasicHttpSecurityMode securityMode = BasicHttpSecurityMode.None;

            BasicHttpBinding binding = new BasicHttpBinding(securityMode);
            binding.TransferMode = TransferMode.Streamed;
            binding.MaxReceivedMessageSize = 500000000;

            EndpointAddress address = new EndpointAddress(url);

            //Create new instance of iStream
            ChannelFactory<IStream> factory = new ChannelFactory<IStream>(binding, address);
            Channel = factory.CreateChannel();
        }

        public void UploadFile(string FilePath)
        {
            try
            {
                //Open file
                using (var inputStream = new FileStream(FilePath, FileMode.Open, FileAccess.Read, FileShare.Read))
                {
                    FileTransferMessage msg = new FileTransferMessage();
                    msg.Filename = Path.GetFileName(FilePath);
                    msg.TransferStream = inputStream;

                    //Upload file using IStream.Upload
                    Channel.Upload(msg);
                }
            }
            catch (Exception Ex)
            {
                Console.WriteLine("Exception : {0}", Ex.Message);
            }
        }

        public void DownloadFile(string SavePath, string Filename)
        {
            int totalBytes = 0;
            try
            {
                //Get stream from iStream.Download Interface
                Stream MyStream = Channel.Download(Filename);
                string FilePath = Path.Combine(SavePath, Filename);

                if (!Directory.Exists(SavePath))
                {
                    Directory.CreateDirectory(SavePath);
                }

                //open stream to read bytes
                using (var outputStream = new FileStream(FilePath, FileMode.Create))
                {
                    while (true)
                    {
                        //Read bytes form stream
                        int bytesRead = MyStream.Read(Block, 0, BlockSize);
                        totalBytes += bytesRead;
                        if (bytesRead > 0)
                            outputStream.Write(Block, 0, bytesRead); //write bytes to file
                        else
                            break;
                    }
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine("Execption : {0}", ex.Message);
            }
        }

        public bool CheckFileAvailability(string Filename)
        {
            return Channel.CheckFileAvailability(Filename);
        }
    }

    [ServiceBehavior(IncludeExceptionDetailInFaults = true)]
    public class FileServiceHost : IStream
    {
        static string RepositoryPath;
        static int BlockSize;
        static byte[] Block;
        ServiceHost host;

        //private FileServiceHost()
        //{
        //    /**
        //     * Cant use parameterized constructor.
        //     * Need a way to pass repository path
        //     * Tried using below function NewHost but we get multiple constructor calls
        //     * So have to define values here.
        //     */
        //    RepositoryPath = @"..\..\..\Repository";
        //    int BlockSize = 1024;
        //    Block = new byte[BlockSize];
        //}

        public static FileServiceHost NewHost(string URL, string RepoPath, int BlkSize = 1024)
        {
            FileServiceHost NewServiceHost = new FileServiceHost();
            RepositoryPath = RepoPath;
            BlockSize = BlkSize;
            Block = new byte[BlockSize];
            //create host with specified url
            NewServiceHost.host = NewServiceHost.CreateServiceChannel(URL);
            //Start new host instance
            NewServiceHost.host.Open();
            return NewServiceHost;
        }

        ~FileServiceHost()
        {
            if (null != host)
            {
                host.Close();
            }
        }

        private ServiceHost CreateServiceChannel(string url)
        {
            BasicHttpBinding binding = new BasicHttpBinding();
            binding.TransferMode = TransferMode.Streamed;
            binding.MaxReceivedMessageSize = 50000000;
            Uri baseAddress = new Uri(url);
            ServiceHost host = new ServiceHost(typeof(FileServiceHost), baseAddress);
            host.AddServiceEndpoint(typeof(IStream), binding, baseAddress);
            return host;
        }

        public void Upload(FileTransferMessage msg)
        {
            int totalBytes = 0;

            string FilePath = Path.Combine(RepositoryPath, msg.Filename);

            //check if local directory exists
            if (!Directory.Exists(RepositoryPath))
            {
                Directory.CreateDirectory(RepositoryPath);
            }

            Console.WriteLine("Saving file ({0}) to Repository at ({1})", msg.Filename, RepositoryPath);

            //open stream
            using (var outputStream = new FileStream(FilePath, FileMode.Create, FileAccess.ReadWrite, FileShare.ReadWrite))
            {
                int bytesRead;
                do
                {
                    //read bytes from stream and write to file
                    bytesRead = msg.TransferStream.Read(Block, 0, BlockSize);
                    totalBytes += bytesRead;
                    if (bytesRead > 0)
                    {
                        outputStream.Write(Block, 0, bytesRead);
                    }
                } while (bytesRead > 0);
            }
        }

        public Stream Download(string Filename)
        {
            string FilePath = Path.Combine(RepositoryPath, Filename);
            FileStream outStream = null;

            if (File.Exists(FilePath))
            {
                //create new stream and return
                outStream = new FileStream(FilePath, FileMode.Open, FileAccess.ReadWrite, FileShare.ReadWrite);
            }
            else
            {
                throw new Exception("Open failed for \"" + Filename + "\"");
            }

            Console.WriteLine("Sendng file ({0}) from Repository location ({1})", Filename, RepositoryPath);

            return outStream;
        }

        public bool CheckFileAvailability(string filename)
        {
            string FilePath = Path.Combine(RepositoryPath, filename);

            if (!File.Exists(FilePath))
            {
                return false;
            }

            return true;
        }

        static void Main(string[] args)
        {
            string FileHostingUrl = @"http://localhost:4000/IStream/FileHosting";

            Thread t = new Thread(url =>
            {
                Console.WriteLine("Creating Server.");
                FileServiceHost FileHost;
                FileHost = FileServiceHost.NewHost((string)url, @"..\..\..\Repository");
            });
            t.Start(FileHostingUrl);

            Console.WriteLine("Creating Client...");
            FileStreamer Streamer = new FileStreamer(FileHostingUrl);

            Console.WriteLine("Uploading File...");
            Streamer.UploadFile(@"..\..\FileStreamer.cs");

            Console.WriteLine("File (FileStreamer.cs) Availibility : {0}", Streamer.CheckFileAvailability("FileStreamer.cs"));
            Console.WriteLine("File (File.cs) Availibility : {0}", Streamer.CheckFileAvailability("File.cs"));

            Console.WriteLine("Downloading File...");
            Streamer.DownloadFile(@"..\..\..\Repository", "FileStreamer.cs");

            t.Join();
        }
    }
}
