Remote Test Harness

Building Solution:
Double Click "build.bat" file.
This will rebuild entire solution in release mode.


Running Solution:
Double Click "Run.bat" file 
This will open following 6 windows
       - Repository
       - Test Harness
       - Client GUI1 Window
       - Client GUI1 Console (To display processing of GUI1 Window)
       - Client GUI2 Window
       - Client GUI2 Console (To display processing of GUI2 Window)

Doing this shows the harnsess accepts multiple concurrent test request from multiple clients :)
You can close one window (Client GUI2 window and console) and test the GUI1... or vice versa. 
--------------------------------------------------------------------------

Demo:

For Demonstration, I have already executed following use cases

1. Test Request (XmlParserTestDriver)
   This test driver tests the XmlParser code used in the test harness.
   It parses "SampleCodeTestRequest.xml" file and verifies its contents.

2. Query for results of above test 

3. Test Request (ExceptionTestDriver)
   This test request is to demonstrate requirement 3. I have programmed code for not sending
   ExceptionTestDriver to repository and directly sending test request to test harness.
   The test harness will return appropriate error.

--------------------------------------------------------------------------------
Using GUI for Demo

1. Testing Code
   The GUI is already set to test a "SampleDriver". Just hit "Submit Test" and it will send the test request.
   Note that the test fails as it simulates a failue case.
   Click on the log in "Test Result" window to see details.

2. Query for Log
   Select the radio button to "Get Test Results" and hit "Get Result"
   Click on the log in "Test Result" window to see the result.

Happy Testing :)



