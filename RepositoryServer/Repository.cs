﻿/**
 * Repository Server
 * A file store that allows for storing and sending files.
 *      
 * FileName     : Repository.cs
 * Author       : Sahil Gupta
 * Date         : 16 November 2016 
 * Version      : 1.0
 * 
 * Public Interface
 * ----------------
 * Repository(string MyUrl, string LocalRepo)
 *  - create new instance of reposiory listening to requests 
 *    at specified URL with localRepo as path to send/receive files
 * void AddFileHost(string FileHostingUrl)
 *  - Creates new host with URL for streaming files using WCF
 * 
 * Build Process
 * -------------
 * - Required files: ITest.cs, BlockingQueue.cs, FileMgr.cs MessageBuilder.cs, MsgParser.cs, Sender.cs, Receiver.cs FileStreamer.cs
 * 
 * Maintenance History
 * -------------------
 * ver 1.0 : 24 September 2016
 *  - first release
 */

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading;

namespace RepositoryServer
{
    using Communication;
    using TestInterface;
    using nsBlockingQueue;
    using FileHosting;
    using FileManager;

    class Repository
    {
        string URL;
        string LocalRepo;

        Thread tSender;
        Sender MySender;
        BlockingQueue<Message> SendQ;

        Thread tReceiver;
        Receiver MyReceiver;
        BlockingQueue<Message> ReceiveQ;

        FileServiceHost FileHost;
        FileMgr Database;

        public Repository(string MyUrl, string LocalRepo)
        {
            Console.WriteLine("REQUIREMENT 11: Starting Repository Server at URL ({0})", MyUrl);
            this.URL = MyUrl;
            this.LocalRepo = LocalRepo;

            Database = new FileMgr(LocalRepo);

            SendQ = new BlockingQueue<Message>();
            MySender = new Sender();
            tSender = new Thread(ThreadSender);
            tSender.Start();

            ReceiveQ = new BlockingQueue<Message>();
            MyReceiver = new Receiver();
            tReceiver = new Thread(ThreadReceiver);
            tReceiver.Start();
        }

        public void AddFileHost(string FileHostingUrl)
        {
            FileHost = FileServiceHost.NewHost(FileHostingUrl, LocalRepo);
        }

        void ThreadSender()
        {
            while (true)
            {
                Message msg = SendQ.deQ();
                Console.WriteLine("\nREQUIREMENT 10: Sending Message ({0}) to ({1}) using WCF.", msg.Type.ToString(), msg.ToUrl);
                MySender.CreateNewSession(msg.ToUrl);
                MySender.SendMessage(msg);
            }
        }

        void ThreadReceiver()
        {
            MyReceiver.Init(URL);
            while (true) {
                Console.WriteLine("Waiting for Messages...");
                Message Msg = MyReceiver.GetMessage();
                Console.WriteLine("\nREQUIREMENT 10: New ({0}) Message received from ({1}) using WCF.", Msg.Type.ToString(), Msg.FromUrl);
                //TODO : Delegate Work to another thread
                if (Msg.Type == Message.MessageType.StoreTestLogMsg) {
                    Console.WriteLine("REQUIREMENT 6 & 7 : Got Test logs from Test Harness.");
                    TestMessage TestMsg = new TestMessage();
                    TestMsg.ParseTestResultXML(Msg.Payload); // parse result from XML in structure
                    string filename = Database.WriteLog(TestMsg.Author, TestMsg.TestDriver, TestMsg.TestName, Msg.Payload, TestMsg.TestStatus ? "PASS" : "FAIL"); //write to file
                    if(null != filename) {
                        Console.WriteLine("REQUIREMENT 8: Storing Test Logs with file name ({0}) including developer id and datetime", filename); 
                    }
                }
                if (Msg.Type == Message.MessageType.GetTestLogMsg) {
                    TestMessage TestMsg = new TestMessage();
                    TestMsg.ParseQueryTestResultXML(Msg.Payload); // parse result from XML in structure
                    Console.WriteLine("REQUIREMENT 9: Query for Test Log for Author({0}) with Test Name({1})", TestMsg.Author, TestMsg.TestName);
                    string TestResult = Database.GetDriverTestResultString(TestMsg.Author, TestMsg.TestName); //Search for test result
                    if (null == TestResult) {
                        TestResult = TestMsg.GetTestResultXML(false, "Invalid Test Name");
                    } //send message containing result in payload
                    MessageBuilder TestLogMsg = new MessageBuilder(Message.MessageType.TestLogsAck);
                    TestLogMsg.BuildMessage(URL, Msg.FromUrl, Msg.Author, TestResult);
                    SendQ.enQ(TestLogMsg.GetMessage());
                }
            }
        }        

        static void Main(string[] args)
        {
            string URL = @"http://localhost:4000/ICommunication/Repository";
            string FileHostingUrl = @"http://localhost:4000/IStream/FileHosting";
            string RepositoryPath = "Repository\\RepositoryServerDir";

            Console.Title = "Repository (" + URL + ")";

            Repository MyRepo = new Repository(URL, RepositoryPath);

            MyRepo.AddFileHost(FileHostingUrl);
        }
    }
}
