﻿/**
 * Message Parser
 * Functions to parse a message
 * 
 * FileName     : MsgParser.cs
 * Author       : Sahil Gupta
 * Date         : 12 October 2016 
 * Version      : 1.0
 * 
 * Public Interface for Message Builder
 * -------------------------------------
 * void ParseMessage(Message msg)
 *  - parse a message and display its fields
 * 
 * Build Process
 * -------------
 * - Required files:   ITest.cs, MessageBuilder.cs
 * 
 * Maintenance History
 * -------------------
 * ver 1.0 : 12 October 2016 
 *  - first release
 */

using System;

namespace Communication
{
    using TestInterface;

    public class MessageParser
    {
        public void ParseMessage(Message msg)
        {
            switch (msg.Type)
            {
                case Message.MessageType.TestRequestMessage:
                case Message.MessageType.GetTestLogMsg:
                case Message.MessageType.StoreTestLogMsg:
                case Message.MessageType.TestLogsAck:
                case Message.MessageType.TestRequestAck:

                    MessageBuilder.DisplayMessage(msg);

                    //Do Work

                    Console.WriteLine("");
                    break;
                default:
                    Console.WriteLine("Unknown Message Type ({0})", msg.Type.ToString());
                    break;
            }
        }

        static void Main(string[] args)
        {
            MessageBuilder FileStoreMsgBuilder = new MessageBuilder(Message.MessageType.GetTestLogMsg);

            string data = "File1";
            FileStoreMsgBuilder.BuildMessage("localhost", "localhost", "Sahil", data);

            MessageParser MsgParser = new MessageParser();

            MsgParser.ParseMessage(FileStoreMsgBuilder.GetMessage());
        }
    }
}
