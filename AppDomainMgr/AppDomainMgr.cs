﻿/**
 * Application Domain Manager
 * Creates application domain for to process individual test request
 * 
 * FileName     : AppDomainMgr.cs
 * Author       : Sahil Gupta
 * Date         : 24 September 2016 
 * Version      : 2.0
 *
 * Public Interface
 * ----------------
 * AppDomainMgr AppDMgr = new AppDomainMgr(RepositoryPath, FileHostingUrl);
 *  - Constructor
 * bool ProcessTestRequest(Message Msg, Action<Message, bool, string> NotifyTestResult)
 *  - Start processing test request in Msg and invoke NotifyTestResult 
 *    to publish test results to interested callers.
 * public void DisplayTestInfoList()
 *  - Display request for individual drivers in test request
 * 
 * Build Process
 * -------------
 * - Required files:   ITest.cs, XMLParser.cs, Loader.cs, FileMgr.cs, FileStreamer.cs
 * - Needs repository server started to fetch test files.
 * 
 * Maintenance History
 * -------------------
 * ver 1.0 : 24 September 2016
 *  - first release
 * ver 2.0 : 20 November 2016
 *  - Added communication path to communicate with repository to get test files
 *  - ProcessTestRequest accepts delegate which is called when test results are generated.
 */

using System;
using System.Runtime.Remoting;
using System.Collections.Generic;
using System.Xml.Linq;
using System.Xml;
using System.Text;
using System.IO;
using System.Reflection;
using System.Threading;

namespace TestHarness
{
    using TestInterface;
    using XMLParser;
    using AssemblyLoader;
    using FileManager;
    using FileHosting;

    class TestInfo
    {
        /**********************************************************************
                                 M E M B E R S
         **********************************************************************/

        public string Version { get; set; }
        public string Author { get; set; }
        public DateTime TimeStamp { get; set; }
        public string TestName { get; set; }
        public string TestDriverDLL { get; set; }
        public string TestDriverClass { get; set; }
        public List<string> TestCodeDLL { get; set; }

        /**********************************************************************
                         P U B L I C   M E T H O D S
         **********************************************************************/

        public TestInfo()
        {
            Version = "2";
        }

        public void Display()
        {
            Console.WriteLine("  {0,-12} : {1}", "Version", Version);
            Console.WriteLine("  {0,-12} : {1}", "Author", Author);
            Console.WriteLine("  {0,-12} : {1}", "TimeStamp", TimeStamp);
            Console.WriteLine("  {0,-12} : {1}", "TestName", TestName);
            Console.WriteLine("  {0,-12} : {1}", "TestDriverDLL", TestDriverDLL);
            Console.WriteLine("  {0,-12} : {1}", "TestDriverClass", TestDriverClass);

            foreach (string DLL in TestCodeDLL)
            {
                Console.WriteLine("  {0,-12} : {1}", "TestCodeDLL", DLL);
            }
            Console.WriteLine("");
        }
    }

    public class AppDomainMgr : MarshalByRefObject
    {
        /**********************************************************************
                                 M E M B E R S
         **********************************************************************/

        private List<TestInfo> TestInfoList { get; set; }

        private string RepositoryPath { get; set; }
        private string FileHostingUrl { get; set; }

        /**********************************************************************
                                 P U B L I C   M E T H O D S
         **********************************************************************/

        public AppDomainMgr(string path, string FileHostingUrl)
        {
            TestInfoList = null;
            RepositoryPath = path;
            this.FileHostingUrl = FileHostingUrl;
        }

        private string GetFilesFromRepositoryServer(string localDirName)
        {
            string localpath = RepositoryPath + "//" + localDirName;
            string FileName = null;
            try {
                Directory.CreateDirectory(localpath);
                FileStreamer Streamer = new FileStreamer(FileHostingUrl);
                foreach (var Test in TestInfoList) {
                    //Check if test driver file is present in repository
                    if (false == Streamer.CheckFileAvailability(Test.TestDriverDLL)) {
                        FileName = Test.TestDriverDLL;
                        break;
                    }
                    //Download Test Driver File from Repository
                    Console.WriteLine("REQUIREMENT 6 : Downloading Test Driver file {0} using Streaming.", Test.TestDriverDLL);
                    Streamer.DownloadFile(localpath, Test.TestDriverDLL);
                    foreach (var Library in Test.TestCodeDLL) {
                        //Check if Library file is available in Repository
                        if (false == Streamer.CheckFileAvailability(Library)) {
                            FileName = Library;
                            break;
                        }
                        //Download Library file from repository
                        Console.WriteLine("REQUIREMENT 6 : Downloading Library file {0} using Streaming.", Library);
                        Streamer.DownloadFile(localpath, Library);
                    }
                }
            } catch (Exception Ex) {
                Console.WriteLine("Exception : ({0})", Ex.Message);
            }
            return FileName;
        }

        private void DeleteFiles(string localDirName)
        {
            string localpath = RepositoryPath + "//" + localDirName;
            foreach (var Test in TestInfoList)
            {
                //Delete test driver file if it exists
                if(File.Exists(Path.Combine(RepositoryPath, Test.TestDriverDLL)))
                {
                    Console.WriteLine("Deleting Test Driver file {0}", Test.TestDriverDLL);
                    File.Delete(Path.Combine(localpath, Test.TestDriverDLL));
                }

                //Delete test code files if they exists    
                foreach (var Library in Test.TestCodeDLL)
                {
                    if (File.Exists(Path.Combine(localpath, Library)))
                    {
                        Console.WriteLine("Deleting Code file {0}", Library);
                        File.Delete(Path.Combine(localpath, Library));
                    }
                }
            }
        }

        private void DeleteDirectory(string localDirName)
        {
            string targetDir = RepositoryPath + "//" + localDirName;
            File.SetAttributes(targetDir, FileAttributes.Normal);

            //get all files and directories form target directory
            string[] files = Directory.GetFiles(targetDir);
            string[] dirs = Directory.GetDirectories(targetDir);

            // Delete all files in directory
            foreach (string file in files)
            {
                File.SetAttributes(file, FileAttributes.Normal);
                File.Delete(file);
            }

            //delete all directories in target directory
            foreach (string dir in dirs)
            {
                DeleteDirectory(dir);
            }

            //delete the target directory
            Directory.Delete(targetDir, false);
        }

        /**
         * ProcessTestRequest
         * Create appdomain and execute test
         */
        public bool ProcessTestRequest(Message Msg, Action<Message, bool, string> NotifyTestResult)
        {
            bool bRet = false;
            AppDomain ChildDomain = null;
            string sTestRequest = Msg.Payload;
            string sTestResult = null;
            try {
                /* Create Application Domain : AppDomain - YYMMDD - HHMMSS - FFF */
                ChildDomain = AppDomain.CreateDomain("AppDomain-" + DateTime.Now.ToString("yyMMdd-HHmmss-fff") + "-" + Thread.CurrentThread.ManagedThreadId);
                Console.WriteLine("REQUIREMENT 4 : New AppDomain ({0}) in Thread ({1})", ChildDomain.FriendlyName, Thread.CurrentThread.ManagedThreadId);
                XmlParser Parser = new XmlParser(); /* Parse Test Request to extract data */
                Parser.ParseXmlString(sTestRequest);
                Console.WriteLine("REQUIREMENT 2 : XML File Specifying developer id, test drivers DLL, code DLL");
                Parser.DisplayTestRequest();
                TestInfoList = GetTestInfoFromXmlTestInfo(Parser._xmlTestInfoList); /* Fill TestInfoList with data from XML */
                sTestResult = GetFilesFromRepositoryServer(ChildDomain.FriendlyName); /* Get Files from Repository */
                if(null != sTestResult) {
                    Console.WriteLine("REQUIREMENT 3 : DLL not found in Repository. Sending Error to Client..");
                    sTestResult = "Error : File " + sTestResult + " Not Found.";
                    return bRet;
                }
                Type tLoader = typeof(Loader); /* Instantiate Loader */
                Loader load = (Loader)ChildDomain.CreateInstanceAndUnwrap(Assembly.GetAssembly(tLoader).FullName,
                              tLoader.ToString(), false, BindingFlags.Default, null, new object[] { RepositoryPath }, null, null);
                foreach (var TestInfo in TestInfoList) { /* Load Assemblies */
                    TestInfo.TestDriverClass = load.LoadAssembly(TestInfo.TestDriverDLL);
                    if (null == TestInfo.TestDriverClass) {
                        Console.WriteLine("load.LoadAssembly({0})...FAILED", TestInfo.TestDriverDLL);
                    }
                } //DisplayTestInfoList();
                bRet = ExecuteTest(ChildDomain, out sTestResult); /* Execute Test in AppDomain */
            } catch (Exception Ex) {
                Console.WriteLine("Exception : {0}", Ex.Message);
                sTestResult = Ex.Message;
            } finally {
                if (null != ChildDomain) {
                    string DirName = ChildDomain.FriendlyName;
                    Console.WriteLine("REQUIREMENT 7: Unloading Child AppDomain ({0})", ChildDomain.FriendlyName);
                    AppDomain.Unload(ChildDomain); /* Unload app domain */
                    DeleteDirectory(DirName);  /* Delete Code Files */
                }
                NotifyTestResult(Msg, bRet, sTestResult);
            }
            return bRet;
        }

        /**
         * ExecuteTest
         * Execute all test requests in child app domain
         */
        private bool ExecuteTest(AppDomain ChildDomain, out string sTestResult)
        {
            bool bTestResult = true;
            StringBuilder sbResult = new StringBuilder();
            FileMgr Database = new FileMgr(RepositoryPath);
            foreach (TestInfo Info in TestInfoList) { /* Execute Test */
                bool TestStatus = false;
                string ResultLog;
                try {
                    sbResult.AppendLine("Testing : " + Info.TestDriverDLL);
                    /* Create instance of test driver in app domain */
                    ITest TestDriverInstance = GetITestInstance(ChildDomain, Info.TestDriverDLL, Info.TestDriverClass);
                    if (null == TestDriverInstance) {
                        Console.WriteLine("Error:GetITestInstance({0})...FAILED.", Info.TestDriverClass);
                        continue;
                    }
                    /* Execute Test */
                    Console.WriteLine("REQUIREMENT 5 : Invoking test() from ITest Interface for driver ({0})", Info.TestDriverDLL);
                    Console.WriteLine(".........................");
                    TestStatus = TestDriverInstance.test();
                    if (TestStatus)
                        Console.WriteLine("<<<Test PASS>>>");
                    else {
                        bTestResult = false;
                        Console.WriteLine("<<<Test FAIL>>>");
                    }
                    Console.WriteLine(".........................");
                    ResultLog = TestDriverInstance.getLog(); /* Get Test Results */
                    Console.WriteLine("REQUIREMENT 5: Invoking getLog() from iTest interface for driver ({0})", Info.TestDriverDLL);
                    Console.WriteLine("{0}", ResultLog);
                } catch (Exception Ex) {
                    ResultLog = "Exception : " + Ex.Message;
                    Console.WriteLine("<<<Test FAIL>>>");
                    Console.WriteLine("Caught Exception : {0}", ResultLog); /* Store exception message as test log */
                    bTestResult = false;
                    TestStatus = false;
                }
                sbResult.AppendLine(ResultLog);
                /* Store Test Result in File */
                string filename = Database.WriteLog(Info.Author, Info.TestDriverDLL, Info.TestDriverClass, ResultLog, TestStatus ? "PASS" : "FAIL");
                Console.WriteLine("REQUIREMENT 8: Storing Test Logs with file name ({0}) including developer id and datetime", filename);
            }
            sTestResult = sbResult.ToString();
            return bTestResult;
        }

        /**
         * GetTestInfoFromXmlTestInfo
         * Parse data from xmlTestInfo and fill in TestInfo
         */
        private List<TestInfo> GetTestInfoFromXmlTestInfo(List<xmlTestInfo> xmlTestInfoList)
        {
            if (0 == xmlTestInfoList.Count)
            {
                return null;
            }

            List<TestInfo> TestInfo = new List<TestInfo>();

            foreach (xmlTestInfo xmlInfo in xmlTestInfoList)
            {
                TestInfo newTestInfo = new TestInfo();
                newTestInfo.Author = xmlInfo._Author;
                newTestInfo.TimeStamp = xmlInfo._TimeStamp;
                newTestInfo.TestName = xmlInfo._TestName;
                newTestInfo.TestDriverDLL = xmlInfo._TestDriver;

                newTestInfo.TestCodeDLL = new List<string>();
                foreach (var DLL in xmlInfo._TestCode)
                {
                    newTestInfo.TestCodeDLL.Add(DLL);
                }

                TestInfo.Add(newTestInfo);
            }

            return TestInfo;
        }

        /**
         * GetITestInstance
         * Create instance of Test Driver in AppDomain
         */
        private ITest GetITestInstance(AppDomain ad, string AssemblyName, string ClassName)
        {
            bool bRet;
            string TestDriverAssemblyPath;

            bRet = Loader.GetFilePath(RepositoryPath, AssemblyName, out TestDriverAssemblyPath);
            if (false == bRet)
            {
                Console.WriteLine("Error: Loader.GetFilePath({0})...FAILED.", AssemblyName);
                return null;
            }

            ObjectHandle oh = ad.CreateInstanceFrom(TestDriverAssemblyPath, ClassName);
            object ob = oh.Unwrap();

            return (ITest)ob;
        }

        /**
         * DisplayTestInfoList
         * Display request for individual drivers in test request
         */
        public void DisplayTestInfoList()
        {
            foreach (var Info in TestInfoList)
            {
                Info.Display();
            }
        }

        public static void Main()
        {
            string RepositoryPath = @"..\..\..\Repository";
            string TestRequest = RepositoryPath + @"\TestRequest\SampleCodeTestRequest.xml";
            string FileHostingUrl = @"http://localhost:4000/IStream/FileHosting";

            AppDomainMgr AppDMgr = new AppDomainMgr(RepositoryPath, FileHostingUrl);

            Message Msg = new Message();
            Msg.Payload =  XDocument.Load(new FileStream(TestRequest, System.IO.FileMode.Open)).ToString();

            Action<Message, bool, string> Display = (MyMsg, Status, TestResult) => 
                Console.WriteLine("Test Status :({0})\nTestResult:\n{1}", Status, TestResult);
            
            AppDMgr.ProcessTestRequest(Msg, Display);
        }
    }
}