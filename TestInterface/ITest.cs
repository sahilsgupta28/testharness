﻿/**
 * Interface
 * Defines ITest Interface which ever test driver must implement
 * 
 * FileName     : ITest.cs
 * Author       : Sahil Gupta
 * Date         : 25 September 2016 
 * Version      : 2.0
 * 
 * Public Interface
 * ----------------
 * ITest
 *  - To test code and get logs
 * ICommunication
 *  - to send messages 
 * IStream
 *  - to send files
 *  
 * Build Process
 * -------------
 * - Required files:   NA
 * 
 * Maintenance History
 * -------------------
 * ver 1.0 : 25 September 2016
 *  - first release
 * ver 2.0 : 20 November 2016
 *  - Added interface to support commmunication between processes to send test request and files
 *  - Added interfaces to support file streaming
 */

using System;
using System.IO;
using System.ServiceModel;
using System.Runtime.Serialization;

namespace TestInterface
{
    public interface ITest
    {
        bool test();
        string getLog();
    }

    [ServiceContract]
    public interface ICommunication
    {
        [OperationContract(IsOneWay = true)]
        void SendMessage(Message msg);

        Message GetMessage();
    }

    [DataContract]
    public class Message
    {
        public enum MessageType
        {
            //[EnumMember]
            //StoreFilesMsg,      //Sent by Client to Repository to store DLL files
            //[EnumMember]
            //LoadFilesMsg,       //Sent by Test Harness to Repository to get DLL files

            [EnumMember]
            TestRequestMessage, //Sent by Client to Test Harness to test given DLL
            [EnumMember]
            TestRequestAck,     //Sent by Test Harness to Client in response to TestRequestMessage

            [EnumMember]
            StoreTestLogMsg,    //Sent by Test Harness to Repository to store test results
            [EnumMember]
            GetTestLogMsg,      //Sent by Client to Repository to retrieve test results
            [EnumMember]
            TestLogsAck,        //Sent by Repository to Client in response to GetTestLogMsg
        }

        [DataMember]
        public string FromUrl { get; set; }

        [DataMember]
        public string ToUrl { get; set; }

        [DataMember]
        public DateTime TimeStamp { get; set; }

        [DataMember]
        public string Author { get; set; }

        [DataMember]
        public MessageType Type { get; set; }

        [DataMember]
        public string Payload { get; set; }
    }

    [ServiceContract]
    public interface IStream
    {
        [OperationContract(IsOneWay = true)]
        void Upload(FileTransferMessage msg);

        [OperationContract]
        Stream Download(string filename);

        [OperationContract]
        bool CheckFileAvailability(string filename);
    }

    [MessageContract]
    public class FileTransferMessage
    {
        [MessageHeader(MustUnderstand = true)]
        public string Filename { get; set; }

        [MessageBodyMember(Order = 1)]
        public Stream TransferStream { get; set; }
    }

}
