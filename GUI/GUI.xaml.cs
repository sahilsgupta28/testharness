﻿/**
 * GUI
 * A GUI that allows to build and send test request and see test result
 *      
 * FileName     : GUI.xaml.cs
 * Author       : Sahil Gupta
 * Date         : 16 November 2016 
 * Version      : 1.0
 * 
 * Public Interface
 * ----------------
 * GUI()
 *  - Create New GUI Instance
 *   
 * Build Process
 * -------------
 * - Required files: ITest.cs, client.cs, MessageBuilder.cs
 * 
 * Maintenance History
 * -------------------
 * ver 1.0 : 16 November 2016
 *  - first release
 */

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Threading;
using System.IO;

using nsClient;
using TestInterface;
using Communication;

namespace ClientGUI
{
    public partial class GUI : Window
    {
        Client MyClient;
        TestResult testResult;
        int MaxTestResultCount;
        static bool Demo;

        public GUI()
        {
            InitializeComponent();
            MaxTestResultCount = 5;
            string MyUrl;
            string[] args = Environment.GetCommandLineArgs();

            //For multiple client GUI instance use different URL
            if (1 == args.Length) {
                MyUrl = @"http://localhost:4000/ICommunication/GUI";
            } else {
                MyUrl = args[1];
            }

            Console.Title = "Client GUI Console (" + MyUrl + ")";
            this.Title = "Client GUI Window (" + MyUrl + ")";

            string TestHarnessUrl = @"http://localhost:4000/ICommunication/TestHarness";
            string RepoUrl = @"http://localhost:4000/ICommunication/Repository";
            string FileHostingUrl = @"http://localhost:4000/IStream/FileHosting";

            try {
                Console.WriteLine("REQUIREMENT 11: Starting client at URL ({0})", MyUrl);
                MyClient = new Client(MyUrl);
                MyClient.ConfigureURL(TestHarnessUrl, RepoUrl, FileHostingUrl);

                if (Demo == false)
                {
                    Demo = true;
                    DemoBasicRequirements();
                }

            } catch (Exception Ex) {
                Console.WriteLine("Exception : {0}", Ex.Message);
            }
        }

        private void DemoBasicRequirements()
        {
            Console.WriteLine("REQUIREMENT 2: Demonstrating Basic Requirement of Sending Test Request using XML Parser");

            TestMessage NewTestRequest = new TestMessage("DemoXMLTest");
            NewTestRequest.Author = "Sahil Gupta";
            NewTestRequest.addDriver(@"Repository\ClientDir\XmlParserTestDriver.dll");
            NewTestRequest.addCode(@"Repository\ClientDir\XmlParser.dll");
            MyClient.SendTestRequest(NewTestRequest);

            Thread.Sleep(500);
            Console.WriteLine("REQUIREMENT 9: Demonstrating Basic Requirement of Sending Client Query");
            TestMessage NewQueryTestRequest = new TestMessage("DemoXMLTest");
            NewQueryTestRequest.Author = "Sahil Gupta";
            MyClient.GetTestResult(NewQueryTestRequest);

            Console.WriteLine("REQUIREMENT 3: Demonstrating Requirement 3 using specific files to be deleted from repository");
            TestMessage NewTestRequest2 = new TestMessage("DemoErrorTest");
            NewTestRequest2.Author = "Sahil Gupta";
            NewTestRequest2.addDriver(@"Repository\ClientDir\ExceptionTestDriver.dll");
            NewTestRequest2.addCode(@"Repository\ClientDir\SampleCode.dll");
            MyClient.SendTestRequest(NewTestRequest2);
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            //Start thread to accept messages sent for test request
            Thread t = new Thread(ProcessTestResult);
            t.Start();

            listBox.HorizontalAlignment = System.Windows.HorizontalAlignment.Center;
        }

        private void ProcessTestResult()
        {
            while (true)
            {
                //wait for message result
                Message Msg = MyClient.ProcessTestResult();

                //Got message result, create test message to read message
                TestMessage tMsg = new TestMessage();
                tMsg.ParseTestResultXML(Msg.Payload);
                tMsg.duration = DateTime.Now.Subtract(Msg.TimeStamp);

                testResult = new TestResult();
                testResult.TestName = tMsg.TestName;
                string testtype = "";
                if (Msg.Type == Message.MessageType.TestRequestAck) testtype = "TestResult :: ";
                if (Msg.Type == Message.MessageType.TestLogsAck) testtype = "QueryResult :: ";
                testResult.DisplayLog = testtype + tMsg.TestName + " :: " + (tMsg.TestStatus ? "PASS" : "FAIL") + " :: " + tMsg.Author;
                testResult.DetailedLog = tMsg.GetDetailedTestResult();
                Console.WriteLine("Updating Test Result Summary : ({0})", testResult.DisplayLog);

                //update listbox on GUI with test result
                Dispatcher.Invoke(
                    new Action<TestResult>(UpdateTestResultWindow),
                    System.Windows.Threading.DispatcherPriority.Background,
                    testResult
                );
            }
        }

        private void UpdateTestResultWindow(TestResult testResult)
        {
            //delete last item from list box if count for max items to display is reached
            if(listBox.Items.Count == MaxTestResultCount)
            {
                listBox.Items.RemoveAt(MaxTestResultCount - 1);
            }

            //add new items to top of list box
            listBox.Items.Insert(0, testResult);
        }

        private void listBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            //get selected item
            var addedItems = e.AddedItems;
            if (addedItems.Count > 0)
            {
                //display details for selected test result
                TestResult selectedItem = addedItems[0] as TestResult;
                string TitleCaption = "Test Result for Test : " + selectedItem.TestName;
                MessageBox.Show(selectedItem.DetailedLog, TitleCaption);
            }
            //de-select that item so it can be selected again
            listBox.UnselectAll();
        }

        private void radio_GetTestResult_Checked(object sender, RoutedEventArgs e)
        {
            label3.Visibility = Visibility.Hidden;
            textBox3.Visibility = Visibility.Hidden;
            button_td.Visibility = Visibility.Hidden;
            label4.Visibility = Visibility.Hidden;
            textBox4.Visibility = Visibility.Hidden;
            button_tc.Visibility = Visibility.Hidden;
            submitbutton.Content = "Get Result";
        }

        private void radio_TestCode_Checked(object sender, RoutedEventArgs e)
        {
            //this is the default action and the GUI is not initialized yet
            //so return
            if (null == label3) return;

            label3.Visibility = Visibility.Visible;
            textBox3.Visibility = Visibility.Visible;
            button_td.Visibility = Visibility.Visible;
            label4.Visibility = Visibility.Visible;
            textBox4.Visibility = Visibility.Visible;
            button_tc.Visibility = Visibility.Visible;
            submitbutton.Content = "Submit Test";
        }

        private void submitbutton_Click(object sender, RoutedEventArgs e)
        {
            if (textBox1.Text == "") { //validate author name and test name should not be null
                MessageBox.Show("Enter Author Name", "Error");
                return;
            }
            if (textBox2.Text == "") {
                MessageBox.Show("Enter Test Name", "Error");
                return;
            }
            TestMessage NewTestRequest = new TestMessage(textBox2.Text);
            NewTestRequest.Author = textBox1.Text;
            if (true == radio_TestCode.IsChecked) {
                string Driver = textBox3.Text.Trim(new Char[] { '\"' });
                if (!File.Exists(Driver)) {
                    MessageBox.Show("Invalid File Path : " + Driver, "Error");
                    return;
                }
                NewTestRequest.addDriver(Driver);

                string[] TestCodes = textBox4.Text.Split(',');
                foreach (var TestCode in TestCodes) { //for multiple test codes, split and add each file
                    string Code = TestCode.Trim(new Char[] { '\"' });
                    if (!File.Exists(Code)) {
                        MessageBox.Show("Invalid File Path : " + Code, "Error");
                        return;
                    }
                    NewTestRequest.addCode(Code);
                }
                Console.WriteLine("\nSending New Test Request");
                MyClient.SendTestRequest(NewTestRequest);
            }
            else /* true == radio_GetTestResult.IsChecked */ {
                Console.WriteLine("\nSending New Test Query");
                MyClient.GetTestResult(NewTestRequest);
            }
        }

        private void button_td_Click(object sender, RoutedEventArgs e)
        {
            // Create OpenFileDialog 
            Microsoft.Win32.OpenFileDialog dlg = new Microsoft.Win32.OpenFileDialog();

            // Set filter for file extension and default file extension 
            dlg.DefaultExt = ".dll";
            dlg.Filter = "DLL Files (*.dll)|*.dll";
            dlg.Title = "File Browser";

            // Display OpenFileDialog by calling ShowDialog method 
            Nullable<bool> result = dlg.ShowDialog();

            // Get the selected file name and display in a TextBox 
            if (result == true)
            {
                // Open document 
                textBox3.Text = "";
                foreach (String file in dlg.FileNames)
                    textBox3.Text += "\"" + file + "\"";
            }
        }

        private void button_tc_Click(object sender, RoutedEventArgs e)
        {
            // Create OpenFileDialog 
            Microsoft.Win32.OpenFileDialog dlg = new Microsoft.Win32.OpenFileDialog();

            // Set filter for file extension and default file extension 
            dlg.DefaultExt = ".dll";
            dlg.Filter = "DLL Files (*.dll)|*.dll";
            dlg.Multiselect = true;
            dlg.Title = "File Browser";

            // Display OpenFileDialog by calling ShowDialog method 
            Nullable<bool> result = dlg.ShowDialog();

            // Get the selected file name and display in a TextBox 
            if (result == true)
            {
                // Open document 
                textBox4.Text = "";
                foreach (String file in dlg.FileNames)
                    textBox4.Text += "\"" + file + "\",";
                textBox4.Text = textBox4.Text.TrimEnd(',');
                Console.WriteLine("TextBox {0}", textBox4.Text);
            }
        }
    }

    public class TestResult
    {
        public string TestName { get; set; }
        public string DisplayLog { get; set; }
        public string DetailedLog { get; set; }
    }
}
